#define VERSION_MAJOR 1
#define VERSION_MINOR 6
#define VERSION_PATCH 0
#define VERSION_STRING "1.6.0"
#define VERSION_NAME "Red Eclipse"
#define VERSION_UNAME "redeclipse"
#define VERSION_VNAME "REDECLIPSE"
#define VERSION_RELEASE "Sunset Edition"
#define VERSION_URL "http://redeclipse.net/"
#define VERSION_COPY "2009-2017"
#define VERSION_DESC "A fun-filled new take on the first-person arena shooter."

#define LAN_PORT 28799
#define MASTER_PORT 28800
#define SERVER_PORT 28801
